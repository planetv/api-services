#!/bin/sh

if [ "$1" = "development" ]; then
    go build .
else
    GIN_MODE=release go build .
fi
