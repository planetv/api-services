#!/bin/sh

PROJECT=planetv
PROJECT_DATABASE_CONTAINER=planetv-database-container-1
PROJECT_CONTAINER=planetv-api-services-container-1
PROJECT_IMAGE=planetv-api-services-image
PROJECT_NETWORK=planetv_network

# Check postgresql database ready or not
check_postgresql_for_test() {
	# Maximum number of attempts to check PostgreSQL readiness
	max_attempts=2

	# Counter for tracking the number of successful checks
	success_count=0

	# Check PostgreSQL readiness with a loop
	while [ $success_count -lt $max_attempts ]; do
		if docker exec ${PROJECT_DATABASE_CONTAINER} pg_isready; then
			echo "PostgreSQL is ready"
			success_count=$((success_count + 1))
			echo "Successful checks: $success_count"
		else
			echo "Waiting for PostgreSQL to become ready..."
		fi
		sleep 2
	done

	# Run the test if PostgreSQL is ready for the specified number of times
	if [ $success_count -eq $max_attempts ]; then
		echo "PostgreSQL is ready for $max_attempts checks. Running the test..."
		if [ "$3" == "" ]; then
			docker exec ${PROJECT_CONTAINER} go test -v /api-services/src/$1/$2.go /api-services/src/$1/$2\_test.go
		else
			docker exec ${PROJECT_CONTAINER} go test -v /api-services/src/$1/$2.go /api-services/src/$1/$3.go /api-services/src/$1/$2\_test.go
		fi
	else
		echo "PostgreSQL is not ready after $max_attempts attempts"
		exit 1
	fi
}

# Build image
build_stable_image() {
	docker buildx build . -t ${PROJECT_IMAGE}:stable --build-arg ENVIRONMENT=stable
}
build_dev_image() {
	docker buildx build . -t ${PROJECT_IMAGE}:dev --build-arg ENVIRONMENT=development
}

# Start and stop docker compose steps
start_stable() {
	docker compose -p ${PROJECT} -f docker-compose-stable.yml up -d
}
start_dev() {
	docker compose -p ${PROJECT} -f docker-compose-dev.yml up -d
}
stop_docker() {
	docker compose -p ${PROJECT} stop
}

# Remove steps
remove_container() {
	docker compose -p ${PROJECT} down
}
remove_all_image() {
	docker rmi ${PROJECT_IMAGE}:stable
	docker rmi ${PROJECT_IMAGE}:dev
}
remove_all() {
	remove_container
	remove_all_image
}

# Rebuild steps
rebuild_stable_all() {
	remove_all
	build_stable_image
	start_stable
}
rebuild_dev_all() {
	remove_all
	build_dev_image
	start_dev
}

# Access docker container
access() {
	docker exec -it ${PROJECT_CONTAINER} sh
}
access_database() {
	docker exec -it ${PROJECT_DATABASE_CONTAINER} psql -U postgres
}

# Check log container
log_api_container() {
	docker logs ${PROJECT_CONTAINER}
}
log_database_container() {
	docker logs ${PROJECT_DATABASE_CONTAINER}
}

# Test service
test_service_auth() {
	check_postgresql_for_test "services" "auth"
}
test_service_blogcategory() {
	check_postgresql_for_test "services" "blogcategory"
}
test_service_blogfile() {
	check_postgresql_for_test "services" "blogfile"
}
test_service_blogtag() {
	check_postgresql_for_test "services" "blogtag"
}
test_service_blogtagfile() {
	check_postgresql_for_test "services" "blogtagfile"
}

# Test controller
test_controller_auth() {
	check_postgresql_for_test "controllers" "auth"
}
test_controller_blogcategory() {
	check_postgresql_for_test "controllers" "blogcategory" "auth"
}
test_controller_blogfile() {
	check_postgresql_for_test "controllers" "blogfile" "auth"
}
test_controller_blogtag() {
	check_postgresql_for_test "controllers" "blogtag" "auth"
}
test_controller_blogtagfile() {
	check_postgresql_for_test "controllers" "blogtagfile" "auth"
}

# Test router
test_router_auth() {
	check_postgresql_for_test "routes" "auth"
}
test_router_blogcategory() {
	check_postgresql_for_test "routes" "blogcategory" "auth"
}
test_router_blogfile() {
	check_postgresql_for_test "routes" "blogfile" "auth"
}
test_router_blogtag() {
	check_postgresql_for_test "routes" "blogtag" "auth"
}
test_router_blogtagfile() {
	check_postgresql_for_test "routes" "blogtagfile" "auth"
}

# Test coverage
test_coverage() {
	# Maximum number of attempts to check PostgreSQL readiness
	max_attempts=2

	# Counter for tracking the number of successful checks
	success_count=0

	# Check PostgreSQL readiness with a loop
	while [ $success_count -lt $max_attempts ]; do
		if docker exec ${PROJECT_DATABASE_CONTAINER} pg_isready; then
			echo "PostgreSQL is ready"
			success_count=$((success_count + 1))
			echo "Successful checks: $success_count"
		else
			echo "Waiting for PostgreSQL to become ready..."
		fi
		sleep 2
	done

	docker exec ${PROJECT_CONTAINER} go test -covermode=set ./... -coverprofile=coverage.out
}
test_convert_coverage() {
	docker exec ${PROJECT_CONTAINER} go tool cover -html=coverage.out -o coverage.html
	docker cp ${PROJECT_CONTAINER}:/api-services/coverage.html ./
}

# Print list of options
print_list() {
	echo "Pass wrong arguments! Here is list of arguments for docker script"
	echo -e "\taccess                       : access docker container"
	echo -e "\taccess-database              : access database docker container"
	echo -e "\tbuild-stable                 : build docker image"
	echo -e "\tbuild-cache                  : build with cache docker image"
	echo -e "\tlog-api                      : log api-services docker container"
	echo -e "\tlog-database                 : log database docker container"
	echo -e "\trebuild-stable-all           : rebuild stable (remove, build and start)"
	echo -e "\trebuild-dev-all              : rebuild dev (remove, build and start)"
	echo -e "\tremove-all                   : remove all (container, network, image)"
	echo -e "\tremove-container             : remove container"
	echo -e "\tremove-all-image             : remove all image"
	echo -e "\tstart-stable                 : start docker compose stable"
	echo -e "\tstart-dev                    : start docker compose dev"
	echo -e "\tstop                         : stop docker compose"
	echo -e "\ttest-service-auth            : test auth service"
	echo -e "\ttest-service-blogcategory    : test blogcategory service"
	echo -e "\ttest-service-blogfile        : test blogfile service"
	echo -e "\ttest-service-blogtag         : test blogtag service"
	echo -e "\ttest-service-blogtagfile     : test blogtagfile service"
	echo -e "\ttest-controller-auth         : test auth controller"
	echo -e "\ttest-controller-blogcategory : test blogcategory controller"
	echo -e "\ttest-controller-blogfile     : test blogfile controller"
	echo -e "\ttest-controller-blogtag      : test blogtag controller"
	echo -e "\ttest-controller-blogtagfile  : test blogtagfile controller"
	echo -e "\ttest-router-auth             : test auth router"
	echo -e "\ttest-router-blogcategory     : test blogcategory router"
	echo -e "\ttest-router-blogfile         : test blogfile router"
	echo -e "\ttest-router-blogtag          : test blogtag router"
	echo -e "\ttest-router-blogtagfile      : test blogtagfile router"
}

# Main script
if [ $# -eq 1 ]; then
	case "$1" in
		"access" )
			access ;;
		"access-database" )
			access_database ;;
		"build-stable" )
			build_stable_image ;;
		"build-dev" )
			build_dev_image ;;
		"log-api" )
			log_api_container ;;
		"log-database" )
			log_database_container ;;
		"rebuild-stable-all" )
			rebuild_stable_all ;;
		"rebuild-dev-all" )
			rebuild_dev_all ;;
		"remove-all" )
			remove_all ;;
		"remove-container" )
			remove_container ;;
		"remove-image" )
			remove_all_image ;;
		"start" )
			start ;;
		"start-dev" )
			start_dev ;;
		"stop" )
			stop_docker ;;
		"test-service-auth" )
			test_service_auth ;;
		"test-service-blogcategory" )
			test_service_blogcategory ;;
		"test-service-blogfile" )
			test_service_blogfile ;;
		"test-service-blogtag" )
			test_service_blogtag ;;
		"test-service-blogtagfile" )
			test_service_blogtagfile ;;
		"test-controller-auth" )
			test_controller_auth ;;
		"test-controller-blogcategory" )
			test_controller_blogcategory ;;
		"test-controller-blogfile" )
			test_controller_blogfile ;;
		"test-controller-blogtag" )
			test_controller_blogtag ;;
		"test-controller-blogtagfile" )
			test_controller_blogtagfile ;;
		"test-router-auth" )
			test_router_auth ;;
		"test-router-blogcategory" )
			test_router_blogcategory ;;
		"test-router-blogfile" )
			test_router_blogfile;;
		"test-router-blogtag" )
			test_router_blogtag;;
		"test-router-blogtagfile" )
			test_router_blogtagfile;;
		"test-coverage" )
			test_coverage ;;
		"test-convert-coverage" )
			test_convert_coverage ;;
		* )
			print_list ;;
	esac
else
	print_list
fi
