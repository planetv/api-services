FROM golang:1.22.3-alpine

WORKDIR /api-services

ARG ENVIRONMENT

# Copy package files and download
COPY go.mod go.sum ./
RUN go mod download

# Copy source code
COPY src ./src
COPY server.go ./
COPY build.sh ./

# Build and run
RUN chmod +x ./build.sh
RUN ./build.sh ${ENVIRONMENT}
ENTRYPOINT ["./planetv-api-services"]
