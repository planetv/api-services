package models

type Blogcategory struct {
	Id   int    `json:"id" gorm:"primaryKey"`
	Name string `json:"name"`
}

func (Blogcategory) TableName() string {
	return "blogcategory"
}
