package models

type Blogtag struct {
	Id              int    `json:"id" gorm:"primaryKey"`
	Blogcategory_id int    `json:"blogcategory_id"`
	Name            string `json:"name"`
}

type BlogtagQuery struct {
	Blogtag
	Blogcategory_name string `json:"blogcategory_name"`
}

func (Blogtag) TableName() string {
	return "blogtag"
}
