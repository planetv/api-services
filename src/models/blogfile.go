package models

import (
	"time"
)

type Blogfile struct {
	Id           int       `json:"id" gorm:"primaryKey"`
	Filename     string    `json:"filename"`
	Created_date time.Time `json:"created_date"`
	Updated_date time.Time `json:"updated_date"`
}

func (Blogfile) TableName() string {
	return "blogfile"
}
