package models

type Blogtagfile struct {
	Id          int `json:"id" gorm:"primaryKey"`
	Blogtag_id  int `json:"blogtag_id"`
	Blogfile_id int `json:"blogfile_id"`
}

type BlogtagfileQuery struct {
	Blogtagfile
	Blogtag_name      string `json:"blogtag_name"`
	Blogfile_filename string `json:"blogfile_filename"`
}

func (Blogtagfile) TableName() string {
	return "blogtagfile"
}
