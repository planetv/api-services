package middlewares

import (
	"planetv-api-services/src/libs"
	"planetv-api-services/src/models"
	"planetv-api-services/src/services"

	"net/http"

	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Lấy cookie từ request
		cookie, err := c.Request.Cookie("token")
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": libs.AUTH_UNSUCCESS_NO_TOKEN})
			c.Abort()
			return
		}

		// Khởi tạo và cấu hình service
		service := services.AuthService{}
		service.New(&models.Credentials{})

		// Kiểm tra xem có phải trả về message
		tokenString := cookie.Value
		errMessage := service.CheckToken(tokenString)
		if errMessage != "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": errMessage})
			c.Abort()
			return
		}

		// Token hợp lệ, tiếp tục xử lý request
		c.Next()
	}
}
