package config

import (
	"context"
	"fmt"
	"os"
)

var CTX context.Context = context.Background()

var (
	// Authentication config
	DEFAULT_USERNAME   string = os.Getenv("DEFAULT_USERNAME")
	DEFAULT_PASSWORD   string = os.Getenv("DEFAULT_PASSWORD")
	DEFAULT_SECRET_KEY string = os.Getenv("DEFAULT_SECRET_KEY")
	BCRYPT_COST        string = os.Getenv("BCRYPT_COST")
	TOKEN_LOGIN_TIME   string = os.Getenv("TOKEN_LOGIN_TIME")

	// API config
	API_IP   string = os.Getenv("API_IP")
	API_PORT string = os.Getenv("API_PORT")
	API_URL  string = os.Getenv(API_IP + ":" + API_PORT)

	// Postgresql database config
	DATABASE_USERNAME string = os.Getenv("DATABASE_USERNAME")
	DATABASE_PASSWORD string = os.Getenv("DATABASE_PASSWORD")
	DATABASE_HOST     string = os.Getenv("DATABASE_HOST")
	DATABASE_PORT     string = os.Getenv("DATABASE_PORT")
	DATABASE_DATABASE string = os.Getenv("DATABASE_DATABASE")
	CONNECT_DB        string = fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Ho_Chi_Minh",
		DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_DATABASE, DATABASE_PORT,
	)
)
