package services

import (
	"fmt"
	"math"

	"planetv-api-services/src/config"
	"planetv-api-services/src/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type BlogtagfileService struct {
	models.Blogtagfile
	DB         *gorm.DB
	connection string
}

func (service *BlogtagfileService) New() {
	service.connection = config.CONNECT_DB
}

func (service *BlogtagfileService) Open() error {
	// Mở kết nối tới database
	db, err := gorm.Open(postgres.Open(service.connection), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("can't connect to database: %w", err)
	}

	// Lưu trữ đối tượng db vào trường db của service
	service.DB = db

	return nil
}

func (service *BlogtagfileService) Close() error {
	// Đóng kết nối database
	sqlDB, _ := service.DB.DB()
	defer sqlDB.Close()
	return nil
}

func (service BlogtagfileService) Count() (int, error) {
	// Lấy dữ liệu đếm giá trị trong bảng
	var valueCount int64 = -1
	result := service.DB.Model(&models.Blogtagfile{}).Select("id").Count(&valueCount)

	// Nếu thành công trả về giá trị và nil
	return int(valueCount), result.Error
}

func (service BlogtagfileService) LastId() (int, error) {
	// Lấy giá trị id cuối
	var returnValue int64 = -1
	result := service.DB.Model(&models.Blogtagfile{}).Select("id").Order("id DESC").Limit(1).Last(&returnValue)

	// Nếu thành công trả về giá trị và nil
	return int(returnValue), result.Error
}

func (service BlogtagfileService) Last() (models.Blogtagfile, error) {
	// Lấy giá trị cuối trong bảng
	item := models.Blogtagfile{}
	result := service.DB.Model(&models.Blogtagfile{}).Last(&item)

	// Nếu thành công trả về giá trị và nil
	return item, result.Error
}

func (service BlogtagfileService) FindAll(limit *int, page *int) ([]models.BlogtagfileQuery, error) {
	// Khai báo dữ liệu
	data := []models.BlogtagfileQuery{}

	// Giới hạn limit
	if *limit < 5 {
		*limit = 5
	} else if *limit > 50 {
		*limit = 50
	}

	// Lấy số lượng để giới hạn page
	count, err := service.Count()
	if err != nil {
		return data, err
	}

	// Tính ceilValue
	ceilValue := 0
	if count == 0 {
		ceilValue = 0
	} else {
		ceilValue = int(math.Ceil(float64(count) / float64(*limit)))
	}

	// Giới hạn page
	if *page >= ceilValue && ceilValue > 0 {
		*page = ceilValue - 1
	} else if *page < 0 {
		*page = 0
	}

	// Lấy toàn bộ dữ liệu trong bảng với limit và page

	result := service.DB.
		Table("blogtagfile").
		Joins("INNER JOIN blogtag ON blogtagfile.blogtag_id = blogtag.id").
		Joins("INNER JOIN blogfile ON blogtagfile.blogfile_id = blogfile.id").
		Select("blogtagfile.id AS id, blogtagfile.blogtag_id AS blogtag_id, blogtagfile.blogfile_id AS blogfile_id, blogtag.name AS blogtag_name, blogfile.filename AS blogfile_filename").
		Limit(*limit).Offset(*limit * *page).Scan(&data)

	// Nếu thành công trả về giá trị và nil
	return data, result.Error
}

func (service BlogtagfileService) Create() error {
	// Nhập dữ liệu
	result := service.DB.Omit("Id").Create(
		&models.Blogtagfile{
			Blogtag_id:  service.Blogtag_id,
			Blogfile_id: service.Blogfile_id,
		},
	)

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogtagfileService) Update() error {
	// Cập nhập dữ liệu
	result := service.DB.Updates(models.Blogtagfile{
		Id:          service.Id,
		Blogtag_id:  service.Blogtag_id,
		Blogfile_id: service.Blogfile_id,
	})

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogtagfileService) Delete() error {
	// Xoá dữ liệu
	result := service.DB.Delete(&models.Blogtagfile{}, service.Id)

	// Nếu thành công trả về nil
	return result.Error
}
