package services

import (
	"fmt"
	"math"

	"planetv-api-services/src/config"
	"planetv-api-services/src/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type BlogfileService struct {
	models.Blogfile
	DB         *gorm.DB
	connection string
}

func (service *BlogfileService) New() {
	service.connection = config.CONNECT_DB
}

func (service *BlogfileService) Open() error {
	// Mở kết nối tới database
	db, err := gorm.Open(postgres.Open(service.connection), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("can't connect to database: %w", err)
	}

	// Lưu trữ đối tượng db vào trường db của service
	service.DB = db

	return nil
}

func (service *BlogfileService) Close() error {
	// Đóng kết nối database
	sqlDB, _ := service.DB.DB()
	defer sqlDB.Close()
	return nil
}

func (service BlogfileService) Count() (int, error) {
	// Lấy dữ liệu đếm giá trị trong bảng
	var valueCount int64 = -1
	result := service.DB.Model(&models.Blogfile{}).Select("id").Count(&valueCount)

	// Nếu thành công trả về giá trị và nil
	return int(valueCount), result.Error
}

func (service BlogfileService) LastId() (int, error) {
	// Lấy giá trị id cuối
	var returnValue int64 = -1
	result := service.DB.Model(&models.Blogfile{}).Select("id").Order("id DESC").Limit(1).Last(&returnValue)

	// Nếu thành công trả về giá trị và nil
	return int(returnValue), result.Error
}

func (service BlogfileService) Last() (models.Blogfile, error) {
	// Lấy giá trị cuối trong bảng
	item := models.Blogfile{}
	result := service.DB.Model(&models.Blogfile{}).Last(&item)

	// Nếu thành công trả về giá trị và nil
	return item, result.Error
}

func (service BlogfileService) FindAll(limit *int, page *int) ([]models.Blogfile, error) {
	// Khai báo dữ liệu
	data := []models.Blogfile{}

	// Giới hạn limit
	if *limit < 5 {
		*limit = 5
	} else if *limit > 50 {
		*limit = 50
	}

	// Lấy số lượng để giới hạn page
	count, err := service.Count()
	if err != nil {
		return data, err
	}

	// Tính ceilValue
	ceilValue := 0
	if count == 0 {
		ceilValue = 0
	} else {
		ceilValue = int(math.Ceil(float64(count) / float64(*limit)))
	}

	// Giới hạn page
	if *page >= ceilValue && ceilValue > 0 {
		*page = ceilValue - 1
	} else if *page < 0 {
		*page = 0
	}

	// Lấy toàn bộ dữ liệu trong bảng với limit và page
	result := service.DB.Find(&data).Limit(*limit).Offset(*limit * *page)

	// Nếu thành công trả về giá trị và nil
	return data, result.Error
}

func (service BlogfileService) Create() error {
	// Nhập dữ liệu
	result := service.DB.Omit("Id", "Created_date", "Updated_date").Create(
		&models.Blogfile{
			Filename: service.Filename,
		},
	)

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogfileService) Update() error {
	// Cập nhập dữ liệu
	result := service.DB.Updates(models.Blogfile{
		Id:           service.Id,
		Filename:     service.Filename,
		Created_date: service.Created_date,
		Updated_date: service.Updated_date,
	})

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogfileService) Delete() error {
	// Xoá dữ liệu
	result := service.DB.Delete(&models.Blogfile{}, service.Id)

	// Nếu thành công trả về nil
	return result.Error
}
