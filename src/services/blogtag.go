package services

import (
	"fmt"
	"math"
	"planetv-api-services/src/config"
	"planetv-api-services/src/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type BlogtagService struct {
	models.Blogtag
	DB         *gorm.DB
	connection string
}

func (service *BlogtagService) New() {
	service.connection = config.CONNECT_DB
}

func (service *BlogtagService) Open() error {
	// Mở kết nối tới database
	db, err := gorm.Open(postgres.Open(service.connection), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("can't connect to database: %w", err)
	}

	// Lưu trữ đối tượng db vào trường db của service
	service.DB = db

	return nil
}

func (service *BlogtagService) Close() error {
	// Đóng kết nối database
	sqlDB, _ := service.DB.DB()
	defer sqlDB.Close()
	return nil
}

func (service BlogtagService) Count() (int, error) {
	// Lấy dữ liệu đếm giá trị trong bảng
	var valueCount int64 = -1
	result := service.DB.Model(&models.Blogtag{}).Select("id").Count(&valueCount)

	// Nếu thành công trả về giá trị và nil
	return int(valueCount), result.Error
}

func (service BlogtagService) LastId() (int, error) {
	// Lấy giá trị id cuối
	var returnValue int64 = -1
	result := service.DB.Model(&models.Blogtag{}).Select("id").Order("id DESC").Limit(1).Last(&returnValue)

	// Nếu thành công trả về giá trị và nil
	return int(returnValue), result.Error
}

func (service BlogtagService) Last() (models.Blogtag, error) {
	// Lấy giá trị cuối trong bảng
	item := models.Blogtag{}
	result := service.DB.Model(&models.Blogtag{}).Last(&item)

	// Nếu thành công trả về giá trị và nil
	return item, result.Error
}

func (service BlogtagService) FindAll(limit *int, page *int) ([]models.BlogtagQuery, error) {
	// Khai báo dữ liệu
	data := []models.BlogtagQuery{}

	// Giới hạn limit
	if *limit < 5 {
		*limit = 5
	} else if *limit > 50 {
		*limit = 50
	}

	// Lấy số lượng để giới hạn page
	count, err := service.Count()
	if err != nil {
		return data, err
	}

	// Tính ceilValue
	ceilValue := 0
	if count == 0 {
		ceilValue = 0
	} else {
		ceilValue = int(math.Ceil(float64(count) / float64(*limit)))
	}

	// Giới hạn page
	if *page >= ceilValue && ceilValue > 0 {
		*page = ceilValue - 1
	} else if *page < 0 {
		*page = 0
	}

	// Lấy toàn bộ dữ liệu trong bảng với limit và page
	result := service.DB.Table("blogtag").
		Joins("INNER JOIN blogcategory ON blogtag.blogcategory_id = blogcategory.id").
		Select("blogtag.id AS id, blogtag.blogcategory_id AS blogcategory_id, blogcategory.name AS blogcategory_name, blogtag.name AS name").
		Limit(*limit).Offset(*limit * *page).Scan(&data)

	// Nếu thành công trả về giá trị và nil
	return data, result.Error
}

func (service BlogtagService) Create() error {
	// Nhập dữ liệu
	result := service.DB.Omit("Id").Create(
		&models.Blogtag{
			Blogcategory_id: service.Blogcategory_id,
			Name:            service.Name,
		},
	)

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogtagService) Update() error {
	// Cập nhập dữ liệu
	result := service.DB.Updates(models.Blogtag{
		Id:              service.Id,
		Blogcategory_id: service.Blogcategory_id,
		Name:            service.Name,
	})

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogtagService) Delete() error {
	// Xoá dữ liệu
	result := service.DB.Delete(&models.Blogtag{}, service.Id)

	// Nếu thành công trả về nil
	return result.Error
}
