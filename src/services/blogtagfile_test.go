package services

import (
	"fmt"
	"regexp"
	"testing"

	"planetv-api-services/src/libs"
	"planetv-api-services/src/models"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Test_Open_BlogtagfileService(t *testing.T) {
	// Khai báo dữ liệu
	service := BlogtagfileService{}

	t.Run("Connect to database unsuccess", func(t *testing.T) {
		// Chạy method Open()
		openErr := service.Open()

		// Kiểm tra lỗi
		assert.Error(t, openErr, libs.TEST_HAVE_ERROR)
	})

	t.Run("Connect to database success", func(t *testing.T) {
		// Khai báo dữ liệu
		service.New()

		// Chạy method Open()
		openErr := service.Open()

		// Kiểm tra lỗi
		assert.NoError(t, openErr, libs.TEST_HAVE_NO_ERROR)
	})
}

func Test_Close_BlogtagfileService(t *testing.T) {
	t.Run("Close database connection success", func(t *testing.T) {
		// Khai báo dữ liệu
		service := BlogtagfileService{}
		service.New()

		// Chạy Open() để kết nối
		openErr := service.Open()
		closeErr := service.Close()

		// Kiểm tra lỗi
		assert.NoError(t, openErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, closeErr, libs.TEST_HAVE_NO_ERROR)
	})
}

func Test_Count_BlogtagfileService(t *testing.T) {
	t.Run("Run Count() success", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Chạy method Count()
		count, err := service.Count()

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.Greater(t, count, 0, libs.TEST_DATA_GREATER_THAN_EXPECTED)
	})
}

func Test_LastId_BlogtagfileService(t *testing.T) {
	t.Run("Run LastId() success", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Chạy method LastId()
		last, err := service.LastId()

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.Greater(t, last, 0, libs.TEST_DATA_GREATER_THAN_EXPECTED)
	})
}

func Test_Last_BlogtagfileService(t *testing.T) {
	t.Run("Run Last() success", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Chạy method Last()
		last, err := service.Last()

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, last, models.Blogtagfile{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, last, libs.TEST_DATA_NOT_EMPTY)
	})
}

func Test_FindAll_BlogtagfileService(t *testing.T) {
	t.Run("Run FindAll() unsuccess with Count() error", func(t *testing.T) {
		// Tạo mock DB và mock
		db, mock, err := sqlmock.New()
		if err != nil {
			t.Fatalf("Failed to create mock database: %v", err)
		}
		defer db.Close()

		// Tạo gorm DB từ mock DB
		gormDB, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{})
		if err != nil {
			t.Fatalf("Failed to create gorm database: %v", err)
		}

		// Thiết lập service với mock DB
		service := BlogtagfileService{DB: gormDB}

		// Thiết lập kỳ vọng câu lệnh SQL và trả về lỗi
		mock.ExpectQuery("SELECT COUNT").WillReturnError(fmt.Errorf("can't get count"))

		// Khai báo dữ liệu query limit và page
		limit := 10
		page := 0

		// Chạy method FindAll()
		_, err = service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.Error(t, err, libs.TEST_HAVE_ERROR)
		assert.ErrorContains(t, err, "can't get count")

		// Kiểm tra xem tất cả các kỳ vọng đã được thực hiện hay chưa
		assert.NoError(t, mock.ExpectationsWereMet(), "There were unfulfilled expectations")
	})

	t.Run("Run FindAll() success with Count() = 0", func(t *testing.T) {
		// Tạo mock DB và mock
		db, mock, err := sqlmock.New()
		if err != nil {
			t.Fatalf("Failed to create mock database: %v", err)
		}
		defer db.Close()

		// Tạo gorm DB từ mock DB
		gormDB, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{})
		if err != nil {
			t.Fatalf("Failed to create gorm database: %v", err)
		}

		// Thiết lập service với mock DB
		service := BlogtagfileService{DB: gormDB}

		// Thiết lập kỳ vọng câu lệnh SQL và trả về lỗi
		rows := sqlmock.NewRows([]string{"count"}).AddRow(0)
		mock.ExpectQuery("SELECT COUNT").WillReturnRows(rows)

		// Thiết lập kỳ vọng cho Find với kết quả rỗng
		findRows := sqlmock.NewRows([]string{"id", "blogtag_id", "blogfile_id", "blogtag_name", "blogfile_filename"})
		for i := 1; i <= 5; i++ {
			findRows.AddRow(i, i, i, fmt.Sprintf("Tag name %d", i), fmt.Sprintf("File filename %d", i))
		}
		mock.ExpectQuery(regexp.QuoteMeta(`SELECT blogtagfile.id AS id, blogtagfile.blogtag_id AS blogtag_id, blogtagfile.blogfile_id AS blogfile_id, blogtag.name AS blogtag_name, blogfile.filename AS blogfile_filename FROM "blogtagfile" INNER JOIN blogtag ON blogtagfile.blogtag_id = blogtag.id INNER JOIN blogfile ON blogtagfile.blogfile_id = blogfile.id LIMIT $1`)).
			WillReturnRows(findRows)

		// Khai báo dữ liệu query limit và page
		limit := 10
		page := 0

		// Chạy method FindAll()
		var data = []models.BlogtagfileQuery{}
		data, err = service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)

		// Kiểm tra xem tất cả các kỳ vọng đã được thực hiện hay chưa
		assert.NoError(t, mock.ExpectationsWereMet(), "There were unfulfilled expectations")
	})

	t.Run("Run FindAll() success with count > limit and page > ceil(count / limit)", func(t *testing.T) {
		// Tạo mock DB và mock
		db, mock, err := sqlmock.New()
		if err != nil {
			t.Fatalf("Failed to create mock database: %v", err)
		}
		defer db.Close()

		// Tạo gorm DB từ mock DB
		gormDB, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{})
		if err != nil {
			t.Fatalf("Failed to create gorm database: %v", err)
		}

		// Thiết lập service với mock DB
		service := BlogtagfileService{DB: gormDB}

		// Thiết lập kỳ vọng câu lệnh SQL và trả về lỗi
		rows := sqlmock.NewRows([]string{"count"}).AddRow(22)
		mock.ExpectQuery("SELECT COUNT").WillReturnRows(rows)

		// Thiết lập kỳ vọng cho Find với kết quả rỗng
		findRows := sqlmock.NewRows([]string{"id", "blogtag_id", "blogfile_id", "blogtag_name", "blogfile_filename"})
		for i := 1; i <= 5; i++ {
			findRows.AddRow(i, i, i, fmt.Sprintf("Tag name %d", i), fmt.Sprintf("File filename %d", i))
		}
		mock.ExpectQuery(regexp.QuoteMeta(`SELECT blogtagfile.id AS id, blogtagfile.blogtag_id AS blogtag_id, blogtagfile.blogfile_id AS blogfile_id, blogtag.name AS blogtag_name, blogfile.filename AS blogfile_filename FROM "blogtagfile" INNER JOIN blogtag ON blogtagfile.blogtag_id = blogtag.id INNER JOIN blogfile ON blogtagfile.blogfile_id = blogfile.id LIMIT $1`)).
			WillReturnRows(findRows)

		// Khai báo dữ liệu query limit và page
		limit := 13
		page := 13

		// Chạy method FindAll()
		var data = []models.BlogtagfileQuery{}
		data, err = service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)

		// Kiểm tra xem tất cả các kỳ vọng đã được thực hiện hay chưa
		assert.NoError(t, mock.ExpectationsWereMet(), "There were unfulfilled expectations")
	})

	t.Run("Run FindAll() success", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu query limit và page
		limit := 10
		page := 0

		// Chạy method FindAll()
		data, err := service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)
	})

	t.Run("Run FindAll() success with limit > 50", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu query limit và page
		limit := 51
		page := 0

		// Chạy method FindAll()
		data, err := service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)
	})

	t.Run("Run FindAll() success with limit < 5", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu query limit và page
		limit := 4
		page := 0

		// Chạy method FindAll()
		data, err := service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)
	})

	t.Run("Run FindAll() success with page < 0", func(t *testing.T) {
		// Khai báo dữ liệu kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu query limit và page
		limit := 10
		page := -1

		// Chạy method FindAll()
		data, err := service.FindAll(&limit, &page)

		// Kiểm tra lỗi
		assert.NoError(t, err, libs.TEST_HAVE_NO_ERROR)
		assert.IsType(t, data, []models.BlogtagfileQuery{}, libs.TEST_DATA_SAME_TYPE_WITH_EXPECTED)
		assert.NotEmpty(t, data, libs.TEST_DATA_NOT_EMPTY)
	})
}

func Test_Create_BlogtagfileService(t *testing.T) {
	t.Run("Run Create() success", func(t *testing.T) {
		// Khai báo dữ liệu để kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu để nhập
		service.Blogtag_id = 2
		service.Blogfile_id = 1

		// Lấy số lượng trước khi nhập
		beforeCount, beforeCountErr := service.Count()

		// Chạy method Create()
		createErr := service.Create()

		// Lấy số lượng sau khi nhập
		afterCount, afterCountErr := service.Count()

		// Lấy dữ liệu resultItem thông qua Last() và tạo input item
		resultItem, resultItemErr := service.Last()
		item := models.Blogtagfile{
			Id:          resultItem.Id,
			Blogtag_id:  service.Blogtag_id,
			Blogfile_id: service.Blogfile_id,
		}

		// Kiểm tra lỗi
		assert.NoError(t, beforeCountErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, createErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, afterCountErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, resultItemErr, libs.TEST_HAVE_NO_ERROR)
		assert.Less(t, beforeCount, afterCount, libs.TEST_DATA_LESS_THAN_EXPECTED)
		assert.Equal(t, item, resultItem, libs.TEST_DATA_EQUAL_WITH_EXPECTED)
	})
}

func Test_Update_BlogtagfileService(t *testing.T) {
	t.Run("Run Update() success", func(t *testing.T) {
		// Khai báo dữ liệu để kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu để cập nhập
		last, lastErr := service.LastId()
		service.Id = last
		service.Blogtag_id = 1
		service.Blogfile_id = 1

		// Lấy số lượng trước khi nhập
		beforeUpdate, beforeUpdateErr := service.Last()

		// Chạy method cần kiểm tra
		updateErr := service.Update()

		// Lấy số lượng sau khi nhập
		afterUpdate, afterUpdateErr := service.Last()

		// Kiểm tra lỗi
		assert.NoError(t, lastErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, beforeUpdateErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, updateErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, afterUpdateErr, libs.TEST_HAVE_NO_ERROR)
		assert.NotEqual(t, beforeUpdate, afterUpdate, libs.TEST_DATA_NOT_EQUAL_WITH_EXPECTED)
	})
}

func Test_Delete_BlogtagfileService(t *testing.T) {
	t.Run("Run Delete() success", func(t *testing.T) {
		// Khai báo dữ liệu để kết nối tới database và đóng khi hết function
		service := BlogtagfileService{}
		service.New()
		service.Open()
		defer service.Close()

		// Khai báo dữ liệu để xóa
		lastId, lastIdErr := service.LastId()
		service.Id = lastId

		// Lấy số lượng trước khi nhập
		beforeCount, beforeCountErr := service.Count()

		// Chạy method Delete()
		deleteErr := service.Delete()

		// Lấy số lượng sau khi nhập
		afterCount, afterCountErr := service.Count()

		// Kiểm tra lỗi
		assert.NoError(t, lastIdErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, beforeCountErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, deleteErr, libs.TEST_HAVE_NO_ERROR)
		assert.NoError(t, afterCountErr, libs.TEST_HAVE_NO_ERROR)
		assert.Greater(t, beforeCount, afterCount, libs.TEST_DATA_GREATER_THAN_EXPECTED)
	})
}
