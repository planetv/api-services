package services

import (
	"fmt"
	"math"

	"planetv-api-services/src/config"
	"planetv-api-services/src/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type BlogcategoryService struct {
	models.Blogcategory
	DB         *gorm.DB
	connection string
}

func (service *BlogcategoryService) New() {
	service.connection = config.CONNECT_DB
}

func (service *BlogcategoryService) Open() error {
	// Mở kết nối tới database
	db, err := gorm.Open(postgres.Open(service.connection), &gorm.Config{})
	if err != nil {
		return fmt.Errorf("can't connect to database: %w", err)
	}

	// Lưu trữ đối tượng db vào trường db của service
	service.DB = db

	return nil
}

func (service *BlogcategoryService) Close() error {
	// Đóng kết nối database
	sqlDB, _ := service.DB.DB()
	defer sqlDB.Close()
	return nil
}

func (service BlogcategoryService) Count() (int, error) {
	// Lấy dữ liệu đếm giá trị trong bảng
	var valueCount int64
	result := service.DB.Model(&models.Blogcategory{}).Select("id").Count(&valueCount)

	// Nếu thành công trả về giá trị và nil
	return int(valueCount), result.Error
}

func (service BlogcategoryService) LastId() (int, error) {
	// Lấy giá trị id cuối
	var returnValue int64
	result := service.DB.Model(&models.Blogcategory{}).Select("id").Order("id DESC").Limit(1).Last(&returnValue)

	// Nếu thành công trả về giá trị và nil
	return int(returnValue), result.Error
}

func (service BlogcategoryService) Last() (models.Blogcategory, error) {
	// Lấy giá trị cuối trong bảng
	item := models.Blogcategory{}
	result := service.DB.Model(&models.Blogcategory{}).Last(&item)

	// Nếu thành công trả về giá trị và nil
	return item, result.Error
}

func (service BlogcategoryService) FindAll(limit *int, page *int) ([]models.Blogcategory, error) {
	// Khai báo dữ liệu
	data := []models.Blogcategory{}

	// Giới hạn limit
	if *limit < 5 {
		*limit = 5
	} else if *limit > 50 {
		*limit = 50
	}

	// Lấy số lượng để giới hạn page
	count, err := service.Count()
	if err != nil {
		return data, err
	}

	// Tính ceilValue
	ceilValue := 0
	if count == 0 {
		ceilValue = 0
	} else {
		ceilValue = int(math.Ceil(float64(count) / float64(*limit)))
	}

	// Giới hạn page
	if *page >= ceilValue && ceilValue > 0 {
		*page = ceilValue - 1
	} else if *page < 0 {
		*page = 0
	}

	// Lấy toàn bộ dữ liệu trong bảng với limit và page
	result := service.DB.
		Limit(*limit).Offset(*limit * *page).Order("id DESC").
		Find(&data)

	// Nếu thành công trả về giá trị và nil
	return data, result.Error
}

func (service BlogcategoryService) Create() error {
	// Nhập dữ liệu
	item := models.Blogcategory{
		Name: service.Name,
	}
	result := service.DB.Omit("id").Create(&item)

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogcategoryService) Update() error {
	// Cập nhập dữ liệu
	result := service.DB.Updates(models.Blogcategory{
		Id:   service.Id,
		Name: service.Name,
	})

	// Nếu thành công trả về nil
	return result.Error
}

func (service BlogcategoryService) Delete() error {
	// Xoá dữ liệu
	item := models.Blogcategory{
		Id: service.Id,
	}
	result := service.DB.Delete(&item)

	// Nếu thành công trả về nil
	return result.Error
}
