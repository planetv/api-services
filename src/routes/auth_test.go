package routes

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"planetv-api-services/src/libs"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func Test_LoginController(t *testing.T) {
	// Khai báo router và đường dẫn
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)

	t.Run("Valid login information", func(t *testing.T) {
		// Chuẩn bị request body
		requestBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		request, err := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(requestBody))
		if err != nil {
			t.Fatalf("failed to create request: %v", err)
		}
		request.Header.Set("Content-Type", "application/json")

		// Thực hiện request
		response := httptest.NewRecorder()
		router.ServeHTTP(response, request)

		// Kiểm tra lỗi
		assert.Equal(t, response.Code, http.StatusOK, "Status code phải bằng với StatusOK")
		assert.Contains(t, response.Body.String(), libs.LOGIN_SUCCESS, "Body phải chứa giá trị LOGIN_SUCCESS")
	})
}

func Test_CheckLogin(t *testing.T) {
	// Khai báo router và đường dẫn
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)

	t.Run("Authorize success", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị request check login
		requestCheckLogin, errCheckLogin := http.NewRequest("GET", "/api/auth/check-login", nil)
		if errCheckLogin != nil {
			t.Fatalf("failed to create request: %v", errCheckLogin)
		}

		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestCheckLogin.AddCookie(cookie)

		// Thực hiện request check login
		responseCheckLogin := httptest.NewRecorder()
		router.ServeHTTP(responseCheckLogin, requestCheckLogin)

		// Kiểm tra lỗi
		assert.Equal(t, responseLogin.Code, http.StatusOK, "Status code login phải bằng với StatusOK")
		assert.Equal(t, responseCheckLogin.Code, http.StatusOK, "Status code check login phải bằng với StatusOK")
		assert.Contains(t, responseCheckLogin.Body.String(), libs.AUTH_SUCCESS, "Body phải chứa giá trị AUTH_SUCCESS")
	})
}

func Test_Logout(t *testing.T) {
	// Khai báo router và đường dẫn
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)

	t.Run("Logout success", func(t *testing.T) {
		// Chuẩn bị request
		request, err := http.NewRequest("DELETE", "/api/auth/logout", nil)
		if err != nil {
			t.Fatalf("failed to create request: %v", err)
		}

		// Thực hiện request
		response := httptest.NewRecorder()
		router.ServeHTTP(response, request)

		// Kiểm tra lỗi
		assert.Equal(t, response.Code, http.StatusOK, "Status code phải bằng với StatusOK")
		assert.Contains(t, response.Body.String(), libs.LOGOUT_SUCCESS, "Body phải chứa giá trị LOGOUT_SUCCESS")
	})
}
