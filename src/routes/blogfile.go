package routes

import (
	"planetv-api-services/src/controllers"
	"planetv-api-services/src/middlewares"

	"github.com/gin-gonic/gin"
)

func BlogfileRoutes(rg *gin.RouterGroup) {
	// Khai báo đường dẫn
	route := rg.Group("/blogfile/")

	// Khai báo controller
	controller := controllers.BlogfileController{}
	controller.New()

	// Các đường dẫn không cần authentication
	route.GET("count", controller.Count)
	route.GET("", controller.FindAll)

	// Các đường dẫn cần authentication
	authRoute := route.Group("")
	authRoute.Use(middlewares.AuthMiddleware())
	{
		authRoute.POST("", controller.Create)
		authRoute.PATCH("", controller.Update)
		authRoute.DELETE(":id", controller.Delete)
	}
}
