package routes

import (
	"planetv-api-services/src/controllers"

	"github.com/gin-gonic/gin"
)

func AuthRoutes(rg *gin.RouterGroup) {
	route := rg.Group("/auth/")

	route.GET("check-login", controllers.CheckLogin)
	route.POST("login", controllers.Login)
	route.DELETE("logout", controllers.Logout)
}
