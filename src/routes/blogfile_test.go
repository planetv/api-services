package routes

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"planetv-api-services/src/controllers"
	"planetv-api-services/src/libs"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func Test_Count_BlogfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	BlogfileRoutes(r)

	t.Run("Count success", func(t *testing.T) {
		// Chuẩn bị request check login
		request, err := http.NewRequest("GET", "/api/blogfile/count", nil)
		if err != nil {
			t.Fatalf("failed to create request: %v", err)
		}

		// Thực hiện request
		response := httptest.NewRecorder()
		router.ServeHTTP(response, request)

		// Kiểm tra status code
		assert.Equal(t, response.Code, http.StatusOK, "Status code Count phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, response.Body.String(), "quantity", "Body phải chứa quantity")
	})
}

func Test_FindAll_BlogfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	BlogfileRoutes(r)

	t.Run("Find all success", func(t *testing.T) {
		// Chuẩn bị request check login
		request, err := http.NewRequest("GET", "/api/blogfile/", nil)
		if err != nil {
			t.Fatalf("failed to create request: %v", err)
		}

		// Thực hiện request
		response := httptest.NewRecorder()
		router.ServeHTTP(response, request)

		// Kiểm tra status code
		assert.Equal(t, response.Code, http.StatusOK, "Status code FindAll phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		expectedContains := []string{"id", "filename", "created_date", "updated_date"}
		for _, expected := range expectedContains {
			if !strings.Contains(response.Body.String(), expected) {
				assert.Contains(t, response.Body.String(), expected, "Body phải chứa "+expected)
			}
		}
	})
}

func Test_Create_BlogfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)
	BlogfileRoutes(r)

	t.Run("Authorize success for creating", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị requestBody create
		requestCreateBody := []byte(`{"filename": "test-blog-with-markdown.md"}`)
		requestCreate, errCreate := http.NewRequest("POST", "/api/blogfile/", bytes.NewBuffer(requestCreateBody))
		if errCreate != nil {
			t.Fatalf("failed to create request: %v", errCreate)
		}

		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestCreate.AddCookie(cookie)

		// Thực hiện request create
		responseCreate := httptest.NewRecorder()
		router.ServeHTTP(responseCreate, requestCreate)

		// Kiểm tra status code
		assert.Equal(t, responseLogin.Code, http.StatusOK, "Status code login phải bằng với StatusOK")
		assert.Equal(t, responseCreate.Code, http.StatusOK, "Status code create phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseCreate.Body.String(), libs.CREATE_SUCCESS, "Body phải chứa giá trị CREATE_SUCCESS")
	})
}

func Test_Update_BlogfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)
	BlogfileRoutes(r)

	// Kết nối và đóng database để chuẩn bị id mới nhất được thêm vào
	controller := controllers.BlogfileController{}
	controller.New()
	controller.Service.Open()
	id, errId := controller.Service.LastId()
	if errId != nil {
		t.Fatalf("failed to get last id: %v", errId)
	}
	controller.Service.Close()

	t.Run("Authorize success for updating", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị request update
		requestUpdateBody := []byte(`{"id": ` + strconv.Itoa(int(id)) + `, "filename": "test-website-blog-with-markdown.md", "created_date": "2024-06-14T00:00:00Z", "updated_date": "2024-06-14T00:00:00Z"}`)
		requestUpdate, errUpdate := http.NewRequest("PATCH", "/api/blogfile/", bytes.NewBuffer(requestUpdateBody))
		if errUpdate != nil {
			t.Fatalf("failed to create request: %v", errUpdate)
		}
		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestUpdate.AddCookie(cookie)

		// Thực hiện request update
		responseUpdate := httptest.NewRecorder()
		router.ServeHTTP(responseUpdate, requestUpdate)

		// Kiểm tra status code
		assert.Equal(t, responseUpdate.Code, http.StatusOK, "Status code update phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseUpdate.Body.String(), libs.UPDATE_SUCCESS, "Body phải chứa giá trị UPDATE_SUCCESS")
	})
}

func Test_Delete_BlogfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	r := router.Group("/api")
	AuthRoutes(r)
	BlogfileRoutes(r)

	// Kết nối và đóng database để chuẩn bị id mới nhất được thêm vào
	controller := controllers.BlogfileController{}
	controller.New()
	controller.Service.Open()
	id, errId := controller.Service.LastId()
	if errId != nil {
		t.Fatalf("failed to get last id: %v", errId)
	}
	controller.Service.Close()

	t.Run("Authorize success for deleting", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/api/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị request delete
		requestDelete, errDelete := http.NewRequest("DELETE", "/api/blogfile/"+strconv.Itoa(int(id)), nil)
		if errDelete != nil {
			t.Fatalf("failed to create request: %v", errDelete)
		}

		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestDelete.AddCookie(cookie)

		// Thực hiện request create
		responseDelete := httptest.NewRecorder()
		router.ServeHTTP(responseDelete, requestDelete)

		// Kiểm tra status code
		assert.Equal(t, responseLogin.Code, http.StatusOK, "Status code login phải bằng với StatusOK")
		assert.Equal(t, responseDelete.Code, http.StatusOK, "Status code delete phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseDelete.Body.String(), libs.DELETE_SUCCESS, "Body phải chứa giá trị DELETE_SUCCESS")
	})
}
