package controllers

import (
	"planetv-api-services/src/libs"
	"planetv-api-services/src/middlewares"

	"bytes"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func Test_Count_BlogtagfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	controller := BlogtagfileController{}
	controller.New()
	router.GET("/blogtagfile/count", controller.Count)

	// Chuẩn bị request check login
	request, err := http.NewRequest("GET", "/blogtagfile/count", nil)
	if err != nil {
		t.Fatalf("failed to create request: %v", err)
	}

	// Thực hiện request
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)

	// Kiểm tra status code
	assert.Equal(t, response.Code, http.StatusOK, "Status code Count phải bằng với StatusOK")

	// Kiểm tra giá trị của Body
	assert.Contains(t, response.Body.String(), "quantity", "Body phải chứa quantity")
}

func Test_FindAll_BlogtagfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	controller := BlogtagfileController{}
	controller.New()
	router.GET("/blogtagfile", controller.FindAll)

	// Chuẩn bị request check login
	request, err := http.NewRequest("GET", "/blogtagfile", nil)
	if err != nil {
		t.Fatalf("failed to create request: %v", err)
	}

	// Thực hiện request
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)

	// Kiểm tra status code
	assert.Equal(t, response.Code, http.StatusOK, "Status code FindAll phải bằng với StatusOK")

	// Kiểm tra giá trị của Body
	expectedContains := []string{"id", "blogtag_id", "blogfile_id", "blogtag_name", "blogfile_filename"}
	for _, expected := range expectedContains {
		if !strings.Contains(response.Body.String(), expected) {
			assert.Contains(t, response.Body.String(), expected, "Body phải chứa "+expected)
		}
	}
}

func Test_Create_BlogtagfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	controller := BlogtagfileController{}
	controller.New()
	router.POST("/auth/login", Login)
	router.POST("/blogtagfile", middlewares.AuthMiddleware(), controller.Create)

	t.Run("Ủy quyền không thành công để nhập", func(t *testing.T) {
		// Chuẩn bị request create
		requestCreateBody := []byte(`{"blogtag_id": 2, "blogfile_id": 1}`)
		requestCreate, errCreate := http.NewRequest("POST", "/blogtagfile", bytes.NewBuffer(requestCreateBody))
		if errCreate != nil {
			t.Fatalf("failed to create request: %v", errCreate)
		}

		// Thực hiện request create
		responseCreate := httptest.NewRecorder()
		router.ServeHTTP(responseCreate, requestCreate)

		// Kiểm tra status code
		assert.Equal(t, responseCreate.Code, http.StatusUnauthorized, "Status code create phải bằng với StatusUnauthorized")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseCreate.Body.String(), libs.AUTH_UNSUCCESS_NO_TOKEN, "Body phải chứa giá trị AUTH_UNSUCCESS_NO_TOKEN")
	})

	t.Run("Ủy quyền thành công để nhập", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị requestBody create
		requestCreateBody := []byte(`{"blogtag_id": 2, "blogfile_id": 1}`)
		requestCreate, errCreate := http.NewRequest("POST", "/blogtagfile", bytes.NewBuffer(requestCreateBody))
		if errCreate != nil {
			t.Fatalf("failed to create request: %v", errCreate)
		}

		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestCreate.AddCookie(cookie)

		// Thực hiện request create
		responseCreate := httptest.NewRecorder()
		router.ServeHTTP(responseCreate, requestCreate)

		// Kiểm tra status code
		assert.Equal(t, responseLogin.Code, http.StatusOK, "Status code login phải bằng với StatusOK")
		assert.Equal(t, responseCreate.Code, http.StatusOK, "Status code create phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseCreate.Body.String(), libs.CREATE_SUCCESS, "Body phải chứa giá trị CREATE_SUCCESS")
	})
}

func Test_Update_BlogtagfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	controller := BlogtagfileController{}
	controller.New()
	router.POST("/auth/login", Login)
	router.PATCH("/blogtagfile", middlewares.AuthMiddleware(), controller.Update)

	// Kết nối và đóng database để chuẩn bị id mới nhất được thêm vào
	controller.Service.Open()
	id, errId := controller.Service.LastId()
	if errId != nil {
		t.Fatalf("failed to get last id: %v", errId)
	}
	controller.Service.Close()

	t.Run("Ủy quyền không thành công để cập nhập", func(t *testing.T) {
		// Chuẩn bị request update
		requestUpdateBody := []byte(`{"id": ` + strconv.Itoa(int(id)) + `, "blogtag_id": 1, "blogfile_id": 1}`)
		requestUpdate, errUpdate := http.NewRequest("PATCH", "/blogtagfile", bytes.NewBuffer(requestUpdateBody))
		if errUpdate != nil {
			t.Fatalf("failed to create request: %v", errUpdate)
		}
		// Thực hiện request delete
		responseUpdate := httptest.NewRecorder()
		router.ServeHTTP(responseUpdate, requestUpdate)

		// Kiểm tra status code
		assert.Equal(t, responseUpdate.Code, http.StatusUnauthorized, "Status code update phải bằng với StatusUnauthorized")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseUpdate.Body.String(), libs.AUTH_UNSUCCESS_NO_TOKEN, "Body phải chứa giá trị AUTH_UNSUCCESS_NO_TOKEN")
	})

	t.Run("Ủy quyền không thành công để cập nhập", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị request update
		requestUpdateBody := []byte(`{"id": ` + strconv.Itoa(int(id)) + `, "blogtag_id": 1, "blogfile_id": 1}`)
		requestUpdate, errUpdate := http.NewRequest("PATCH", "/blogtagfile", bytes.NewBuffer(requestUpdateBody))
		if errUpdate != nil {
			t.Fatalf("failed to create request: %v", errUpdate)
		}
		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestUpdate.AddCookie(cookie)

		// Thực hiện request update
		responseUpdate := httptest.NewRecorder()
		router.ServeHTTP(responseUpdate, requestUpdate)

		// Kiểm tra status code
		assert.Equal(t, responseUpdate.Code, http.StatusOK, "Status code update phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseUpdate.Body.String(), libs.UPDATE_SUCCESS, "Body phải chứa giá trị UPDATE_SUCCESS")
	})
}

func Test_Delete_BlogtagfileController(t *testing.T) {
	// Chuẩn bị router
	gin.SetMode(gin.TestMode)
	router := gin.Default()
	controller := BlogtagfileController{}
	controller.New()
	router.POST("/auth/login", Login)
	router.DELETE("/blogtagfile/:id", middlewares.AuthMiddleware(), controller.Delete)

	// Kết nối và đóng database để chuẩn bị id mới nhất được thêm vào
	controller.Service.Open()
	id, errId := controller.Service.LastId()
	if errId != nil {
		t.Fatalf("failed to get last id: %v", errId)
	}
	controller.Service.Close()

	t.Run("Ủy quyền không thành công để xoá", func(t *testing.T) {
		// Chuẩn bị request delete
		requestDelete, errDelete := http.NewRequest("DELETE", "/blogtagfile/"+strconv.Itoa(int(id)), nil)
		if errDelete != nil {
			t.Fatalf("failed to create request: %v", errDelete)
		}
		// Thực hiện request delete
		responseDelete := httptest.NewRecorder()
		router.ServeHTTP(responseDelete, requestDelete)

		// Kiểm tra status code
		assert.Equal(t, responseDelete.Code, http.StatusUnauthorized, "Status code delete phải bằng với StatusUnauthorized")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseDelete.Body.String(), libs.AUTH_UNSUCCESS_NO_TOKEN, "Body phải chứa giá trị AUTH_UNSUCCESS_NO_TOKEN")
	})

	t.Run("Ủy quyền thành công để xóa", func(t *testing.T) {
		// Chuẩn bị request body login
		requestLoginBody := []byte(`{"username": "admin", "password": "admin_password"}`)
		requestLogin, errLogin := http.NewRequest("POST", "/auth/login", bytes.NewBuffer(requestLoginBody))
		if errLogin != nil {
			t.Fatalf("failed to create request: %v", errLogin)
		}
		requestLogin.Header.Set("Content-Type", "application/json")

		// Thực hiện request login
		responseLogin := httptest.NewRecorder()
		router.ServeHTTP(responseLogin, requestLogin)

		// Chuẩn bị request delete
		requestDelete, errDelete := http.NewRequest("DELETE", "/blogtagfile/"+strconv.Itoa(int(id)), nil)
		if errDelete != nil {
			t.Fatalf("failed to create request: %v", errDelete)
		}

		// Trích xuất cookie từ phản hồi sau khi đăng nhập
		cookie := responseLogin.Result().Cookies()[0]

		// Thêm cookie vào yêu cầu kiểm tra đăng nhập
		requestDelete.AddCookie(cookie)

		// Thực hiện request create
		responseDelete := httptest.NewRecorder()
		router.ServeHTTP(responseDelete, requestDelete)

		// Kiểm tra status code
		assert.Equal(t, responseLogin.Code, http.StatusOK, "Status code login phải bằng với StatusOK")
		assert.Equal(t, responseDelete.Code, http.StatusOK, "Status code delete phải bằng với StatusOK")

		// Kiểm tra giá trị của Body
		assert.Contains(t, responseDelete.Body.String(), libs.DELETE_SUCCESS, "Body phải chứa giá trị DELETE_SUCCESS")
	})
}
