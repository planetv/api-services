package controllers

import (
	"planetv-api-services/src/libs"
	"planetv-api-services/src/models"
	"planetv-api-services/src/services"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type BlogtagfileController struct {
	Service services.BlogtagfileService
}

func (controller *BlogtagfileController) New() {
	controller.Service.New()
}

func (controller BlogtagfileController) Count(c *gin.Context) {
	// Mở và đóng database sau khi hoàn thành
	controller.Service.Open()
	defer controller.Service.Close()

	// Thực hiện câu lệnh
	data, err := controller.Service.Count()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": libs.GET_DATA_UNSUCCESS})
		return
	}

	// Trả về dữ liệu
	c.JSON(http.StatusOK, gin.H{"quantity": data})
}

func (controller BlogtagfileController) FindAll(c *gin.Context) {
	// Lấy dữ liệu limit từ query
	limit, err := strconv.Atoi(c.Query("limit"))
	if err != nil {
		limit = 10
	}
	if limit < 10 {
		limit = 10
	} else if limit > 50 {
		limit = 50
	}

	// Lấy dữ liệu page từ query
	page, err := strconv.Atoi(c.Query("page"))
	if err != nil {
		page = 1
	}
	if page < 1 {
		page = 1
	}

	// Mở và đóng database sau khi hoàn thành
	controller.Service.Open()
	defer controller.Service.Close()

	// Thực hiện câu lệnh
	data, err := controller.Service.FindAll(&limit, &page)
	if err != nil {
		response := map[string]string{"message": libs.GET_DATA_UNSUCCESS}
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	// Trả về dữ liệu
	c.JSON(http.StatusOK, data)
}

func (controller BlogtagfileController) Create(c *gin.Context) {
	// Lấy dữ liệu từ body
	inputdata := models.Blogtagfile{}
	if err := c.ShouldBindJSON(&inputdata); err != nil {
		response := map[string]string{"message": libs.INPUT_ERROR}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// Mở và đóng database sau khi hoàn thành
	controller.Service.Open()
	defer controller.Service.Close()

	// Khai báo dữ liệu và thực hiện câu lệnh
	controller.Service.Blogtag_id = inputdata.Blogtag_id
	controller.Service.Blogfile_id = inputdata.Blogfile_id
	err := controller.Service.Create()

	// Bắt lỗi
	if err != nil {
		response := map[string]string{"message": libs.CREATE_UNSUCCESS}
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	// Trả về dữ liệu
	response := map[string]string{"message": libs.CREATE_SUCCESS}
	c.JSON(http.StatusOK, response)
}

func (controller BlogtagfileController) Update(c *gin.Context) {
	// Lấy dữ liệu từ body
	inputdata := models.Blogtagfile{}
	if err := c.ShouldBindJSON(&inputdata); err != nil {
		response := map[string]string{"message": libs.INPUT_ERROR}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// Mở và đóng database sau khi hoàn thành
	controller.Service.Open()
	defer controller.Service.Close()

	// Khai báo dữ liệu và thực hiện câu lệnh
	controller.Service.Id = inputdata.Id
	controller.Service.Blogtag_id = inputdata.Blogtag_id
	controller.Service.Blogfile_id = inputdata.Blogfile_id
	err := controller.Service.Update()

	// Bắt lỗi
	if err != nil {
		response := map[string]string{"message": libs.UPDATE_UNSUCCESS}
		c.SecureJSON(http.StatusInternalServerError, response)
		return
	}

	// Trả về dữ liệu
	response := map[string]string{"message": libs.UPDATE_SUCCESS}
	c.JSON(http.StatusOK, response)
}

func (controller BlogtagfileController) Delete(c *gin.Context) {
	// Lấy dữ liệu từ param id
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		response := map[string]string{"message": libs.INPUT_ERROR}
		c.JSON(http.StatusBadRequest, response)
		return
	}

	// Mở và đóng database sau khi hoàn thành
	controller.Service.Open()
	defer controller.Service.Close()

	// Khai báo dữ liệu và thực hiện câu lệnh
	controller.Service.Id = id
	err = controller.Service.Delete()

	// Bắt lỗi
	if err != nil {
		response := map[string]string{"message": libs.DELETE_UNSUCCESS}
		c.JSON(http.StatusInternalServerError, response)
		return
	}

	// Trả về dữ liệu
	response := map[string]string{"message": libs.DELETE_SUCCESS}
	c.JSON(http.StatusOK, response)
}
