package controllers

import (
	"planetv-api-services/src/config"
	"planetv-api-services/src/libs"
	"planetv-api-services/src/models"
	"planetv-api-services/src/services"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func Login(c *gin.Context) {
	creds := models.Credentials{}
	if err := c.ShouldBindJSON(&creds); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": libs.AUTH_UNSUCCESS_INFORMATION_INVALID})
		return
	}

	// Kiểm tra username
	if creds.Username != config.DEFAULT_USERNAME || creds.Password != config.DEFAULT_PASSWORD {
		c.JSON(http.StatusUnauthorized, gin.H{"error": libs.AUTH_UNSUCCESS_INFORMATION_INVALID})
		return
	}

	// Lấy bcryptCost và mã hóa mật khẩu bằng bcrypt
	bcryptCost, err := strconv.Atoi(config.BCRYPT_COST)
	if err != nil {
		bcryptCost = bcrypt.DefaultCost
	}

	// Tạo mã hóa mật khẩu nhập vào
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(creds.Password), bcryptCost)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": libs.SERVER_ERROR})
		return
	}

	// So sánh mã hóa giữa mật khẩu chính và mật khẩu nhập vào
	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(config.DEFAULT_PASSWORD))
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": libs.AUTH_UNSUCCESS_INFORMATION_INVALID})
		return
	}

	// Khởi tạo và cấu hình service
	service := services.AuthService{}
	service.New(&creds)

	// Khởi tạo token và kiểm tra lỗi
	token, err := service.GenerateToken()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": libs.AUTH_UNSUCCESS_CANT_GENERATE_TOKEN})
		return
	}

	// Chuyển đổi expirationTime.Unix() sang kiểu int
	expirationSeconds := int(service.ExpirationTime.Unix())

	// Tạo cookie từ token và gửi về cho client
	c.SetCookie("token", token, expirationSeconds, "/", config.API_IP, false, true)

	c.JSON(http.StatusOK, gin.H{"message": libs.LOGIN_SUCCESS})
}

func CheckLogin(c *gin.Context) {
	// Lấy token từ cookie
	cookie, err := c.Cookie("token")
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": libs.AUTH_UNSUCCESS_NO_TOKEN})
		return
	}

	// Khởi tạo và cấu hình service
	service := services.AuthService{}
	service.New(&models.Credentials{})

	// Kiểm tra xem có phải trả về message
	errMessage := service.CheckToken(cookie)
	if errMessage != "" {
		c.JSON(http.StatusUnauthorized, gin.H{"error": errMessage})
		return
	}

	// Token hợp lệ và thông tin xác thực đúng, tiến hành xử lý yêu cầu
	c.JSON(http.StatusOK, gin.H{"message": libs.AUTH_SUCCESS})
}

func Logout(c *gin.Context) {
	// Xóa cookie token
	c.SetCookie("token", "", -1, "/", config.API_IP, false, true)

	c.JSON(http.StatusOK, gin.H{"message": libs.LOGOUT_SUCCESS})
}
