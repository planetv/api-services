package main

import (
	"log"
	"time"

	"planetv-api-services/src/config"
	"planetv-api-services/src/routes"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	// Router config
	router := gin.Default()

	// Use Cors middleware
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost:5703"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	r := router.Group("/api")
	routes.AuthRoutes(r)
	routes.BlogcategoryRoutes(r)
	routes.BlogfileRoutes(r)
	routes.BlogtagRoutes(r)
	routes.BlogtagfileRoutes(r)

	// Chạy server và báo lỗi nếu có
	if err := router.Run(":" + config.API_PORT); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}
